package com.vitekey;

import com.gspinka.beans.Cliente;
import com.gspinka.beans.DetalleComprobanteList;
import com.gspinka.beans.Domicilio;
import com.gspinka.beans.DomicilioEntrega;
import com.gspinka.beans.Emisor;
import com.gspinka.beans.Factura;
import com.gspinka.beans.LeyendaList;
import com.gspinka.beans.RelacionadoLlist;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gspinka.beans.Relacionados;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
/**
 *
 * @author armando.rojas
 */
public class Main {

    public static void main(String arg[])
            throws SQLException, ClassNotFoundException, IOException, UnirestException {
        /*=================================Conexion=========================================================================*/
        Properties props = new Properties();
        props.setProperty("user", "scott");
        props.setProperty("password", "tiger");

        Class.forName("oracle.jdbc.driver.OracleDriver");
        

        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@172.20.0.7:1521:SIDGPS", "sysgps_demo", "sysgps_demo");
        
        String sql = "SELECT CDCC_SECUENCIA, NDCC_PEDIDOOIH, cli.E_MAIL, NDCC_SERIE, NDCC_PREIMPRESO, CDCC_TIPODOC, FDCC_EMISION, CDCC_CLIENTE, XDCC_CLIENTE, CDCC_MONEDA,SDCC_STATUS, MDCC_AFECTO, MDCC_NOAFECTO, MDCC_IGV, (MDCC_MONTO + NVL(MDCC_DETRACCION,0)) MONTO_TOTAL, t.NUM1 as IGV, XDCC_CARACTERIS3 FROM DOCUMENTO_CXC LEFT JOIN CLIENTE cli ON cli.COD_CLIENTE = CDCC_CLIENTE INNER JOIN TABLAS t ON t.CATEGORIA = '051' AND t.LLAVE = '03' WHERE CDCC_COMPANIA='01' AND SDCC_STATUS != 'AN' AND XDCC_CARACTERIS3 IS NULL  ORDER BY CDCC_SECUENCIA ASC";
        
       PreparedStatement ps_cabecera = conn.prepareStatement(sql);

        ResultSet result = ps_cabecera.executeQuery();
        
        /*====================================================================================================================*/
        Factura bean = new Factura();
        Cliente beanCliente = new Cliente();
        Domicilio beanDomicilioCliente = new Domicilio();
        bean.setDomicilioEntrega(beanDomicilioCliente);
        beanDomicilioCliente.setCodigoPais("+51");
        beanDomicilioCliente.setDirección("Carr Panamericana Norte SN FND fundo iris sub lote 3");
        beanDomicilioCliente.setUbigeoCodigo("14");
        beanCliente.setDomicilio(beanDomicilioCliente);
        bean.setCliente(beanCliente);
        beanCliente.setEmail("armando.rojas@gpsinka.com");
        beanCliente.setNombreComercial("VITAC & RENTA CAR S.A.C");
        beanCliente.setNumeroDocumento("20522727456");
        beanCliente.setRazonSocial("VITAC & RENTA CAR S.A.C");
        beanCliente.setTipoDocumento("Ruc");
        beanCliente.setCodigoPercepcion("");
        beanCliente.setCodigoRegistroPercepcion("");
        /*bloque json--------------------------*/
        DetalleComprobanteList beanDetalleComprobanteList = new DetalleComprobanteList();
        bean.setDetalleComprobanteList(beanDetalleComprobanteList);
        beanDetalleComprobanteList.setCantidadUnidadItem(0);
        beanDetalleComprobanteList.setCodigoProducto("");
        beanDetalleComprobanteList.setCodigoProductoSUNAT("");
        beanDetalleComprobanteList.setCodigoTipoPrecioVentaItem("");
        beanDetalleComprobanteList.setCodigoUnidadMedida("");
        beanDetalleComprobanteList.setDescripcionItem("");
        beanDetalleComprobanteList.setDescripcionItemCDATA("");
        beanDetalleComprobanteList.setLinea(0);
        beanDetalleComprobanteList.setMontoDescuentoItem(0.0);
        beanDetalleComprobanteList.setMontoIGVItem(0.0);
        beanDetalleComprobanteList.setMontoSCItem(0.0);
        beanDetalleComprobanteList.setMontoPrecioVentaItem(0.0);
        beanDetalleComprobanteList.setMontoValorUnitario(0.0);
        beanDetalleComprobanteList.setMontoValorUnitarioGratuito(0.0);
        beanDetalleComprobanteList.setMontoValorVentaItem(0.0);
        beanDetalleComprobanteList.setNumeroPlacaItem("");
        beanDetalleComprobanteList.setTipoAfectacionIGV("");
        beanDetalleComprobanteList.setTipoCodigoPlaca("");
        beanDetalleComprobanteList.setTipoSistemaCalculoISC("");
        /*____________________________________*/
        beanCliente.setDetraccion(true);
        Domicilio beanDomicilioEntrega = new Domicilio();
        bean.setDomicilioEntrega(beanDomicilioEntrega);
        beanDomicilioEntrega.setCodigoPais("");
        beanDomicilioEntrega.setDirección("");
        beanDomicilioEntrega.setUbigeoCodigo("");
        /*----------------------------------------------*/
        Domicilio beanDomicilioEmisor = new Domicilio();
        bean.setDomicilioEntrega(beanDomicilioEmisor);
        beanDomicilioEmisor.setCodigoPais("+51");
        beanDomicilioEmisor.setDirección("Carr Paranamericana Norte SN FND Fundo Iris Sub Lote 3");
        beanDomicilioEmisor.setUbigeoCodigo("14");
        Emisor beanEmisor = new Emisor();
        bean.setEmisor(beanEmisor);
        beanEmisor.setEmail("imp.genecia@gpsinka.com");
        beanEmisor.setNombreComercial("SOCIEDAD DE AUTOMOTORES INKA S.A.C");
        beanEmisor.setNumeroDocumento("20480683839");
        beanEmisor.setRazonSocial("SOCIEDAD DE AUTOMOTORES INKA S.A.C");
        beanEmisor.setTipoDocumento("RUC");
        beanEmisor.setFechaVencimiento("2017-12-15");
        beanEmisor.setFechaEmision("2017-12-15");
        /*====================================*/
        LeyendaList beanLeyendaList = new LeyendaList();
        bean.setLeyendaList(beanLeyendaList);
        beanLeyendaList.setCodigo("");
        beanLeyendaList.setDescripcion("");
        beanLeyendaList.setMontoBaseImponiblePercepcion(0.0);
        beanLeyendaList.setMontoDescuento(0.0);
        beanLeyendaList.setMontoIGV(0.0);
        beanLeyendaList.setMontoISC(0.0);
        beanLeyendaList.setMontoImporteVentas(0.0);
        beanLeyendaList.setMontoOperacionesExoneradas(0.0);
        beanLeyendaList.setMontoOperacionesGratuitas(0.0);
        beanLeyendaList.setMontoOperacionesGravadas(0.0);
        beanLeyendaList.setMontoOtrosTributos(0.0);
        beanLeyendaList.setMontoPercepcion(0.0);
        beanLeyendaList.setMontoTotalIncPercepcion(0.0);
        beanLeyendaList.setNumero("");
        beanLeyendaList.setNumeroComprobante("");
        //===========================================//
        RelacionadoLlist beanRelacionadoList = new RelacionadoLlist();
        bean.setRelacionadoList(beanRelacionadoList);
        beanRelacionadoList.setEmisorNumeroDocumento("");
        beanRelacionadoList.setEmisorTipoDocumento("");
        beanRelacionadoList.setIndicador("");
        beanRelacionadoList.setMonto(0.0);
        beanRelacionadoList.setNumeroDocumento("");
        beanRelacionadoList.setTipoDocumento("");
        //==========================================/
        beanDetalleComprobanteList.setSerie("");
        beanDetalleComprobanteList.setSumaOtrosCargos(0.0);
        beanDetalleComprobanteList.setTipoComprobante("");
        beanDetalleComprobanteList.setTipoMoneda("");
        beanDetalleComprobanteList.setTipoOperacion("");
        
        Gson gson= new Gson();
        String DomicilioJson=gson.toJson(beanDomicilioCliente);
        System.out.println(DomicilioJson);
        //Domicilio beanDomicilioCliente=gson.fromJson(DomicilioJson, Domicilio.class);
        System.out.println("codigopais:"+ beanDomicilioCliente.getCodigoPais());
        System.out.println("direccion:"+ beanDomicilioCliente.getDirección());
        System.out.println("codigoUbigeo:"+beanDomicilioCliente.getUbigeoCodigo());
        String ClienteJson=gson.toJson(beanCliente);
        System.out.println(ClienteJson);
        //Cliente beanCliente=gson.fromJson(ClienteJson, Cliente.class);
        System.out.println("email:"+ beanCliente.getEmail());
        System.out.println("nombreComercial:"+beanCliente.getNombreComercial());
        System.out.println("numeroDocumento:"+beanCliente.getNumeroDocumento());
        System.out.println("razonSocial:"+beanCliente.getRazonSocial());
        System.out.println("tipoDocumento:"+beanCliente.getTipoDocumento());
        System.out.println("codigoPrecepcion:"+beanCliente.getCodigoPercepcion());
        System.out.println("codigoRegistroPercepcion:"+beanCliente.getCodigoRegistroPercepcion());
        /*==================================================*/
        String DetalleComprobanteListJson=gson.toJson(beanDetalleComprobanteList);
        System.out.println(DetalleComprobanteListJson);
       //DetalleComprobanteList beanDetalleComprobanteList1=gson.fromJson(DetalleComprobanteListJson, DetalleComprobanteList.class);
        System.out.println("cantidadUnidadItem:"+beanDetalleComprobanteList.getCantidadUnidadItem());
        System.out.println("codigoProducto:"+beanDetalleComprobanteList.getCodigoProducto());
        System.out.println("codigoProductoSUNAT:"+beanDetalleComprobanteList.getCodigoProductoSUNAT());
        System.out.println("codigoTipoPrecioVentaItem:"+beanDetalleComprobanteList.getCodigoTipoPrecioVentaItem());
        System.out.println("codigoUnidadMedida:"+beanDetalleComprobanteList.getCodigoUnidadMedida());
        System.out.println("descripcionItem:"+beanDetalleComprobanteList.getDescripcionItem());
        System.out.println("descripcionItemCDATA:"+beanDetalleComprobanteList.getDescripcionItemCDATA());
        System.out.println("linea:"+beanDetalleComprobanteList.getLinea());
        System.out.println("montoDescuentoItem:"+beanDetalleComprobanteList.getMontoDescuentoItem());
        System.out.println("montoIGVItem:"+beanDetalleComprobanteList.getMontoIGVItem());
        System.out.println("montoISCItem:"+beanDetalleComprobanteList.getMontoSCItem());
        System.out.println("montoPrecioVentaItem:"+beanDetalleComprobanteList.getMontoPrecioVentaItem());
        System.out.println("montoValorUnitario:"+beanDetalleComprobanteList.getMontoValorUnitario());
        System.out.println("montoValorUnitarioGratuito:"+beanDetalleComprobanteList.getMontoValorUnitarioGratuito());
        System.out.println("montoValorVentaItem:"+beanDetalleComprobanteList.getMontoValorVentaItem());
        System.out.println("numeroPlacaItem:"+beanDetalleComprobanteList.getNumeroPlacaItem());
        System.out.println("tipoAfectacionIGV:"+beanDetalleComprobanteList.getTipoAfectacionIGV());
        System.out.println("tipoCodigoPlaca:"+beanDetalleComprobanteList.getTipoCodigoPlaca());
        System.out.println("tipoSistemaCalculoISC:"+beanDetalleComprobanteList.getTipoSistemaCalculoISC());
        /*Arreglo*/
        ArrayList<DetalleComprobanteList>detalleCompranteListAL=new ArrayList<DetalleComprobanteList>();
        detalleCompranteListAL.add(beanDetalleComprobanteList);
        detalleCompranteListAL.add(new DetalleComprobanteList());
        String DetalleComprobanteLisJsonAL=gson.toJson(detalleCompranteListAL);
        System.out.println(DetalleComprobanteLisJsonAL);
        /*----------------------------------------------*/
        //Type DetalleComprobanteLisJsonALType=new TypeToken<ArrayList<DetalleComprobanteList>>(){}.getType();
        //ArrayList<DetalleComprobanteList>DetalleComprobanteLisJsonAL2=gson.fromJson(DetalleComprobanteLisJsonAL, ArrayList.class);
        //DetalleComprobanteList beanComprobanteList=DetalleComprobanteLisJsonAL2.get(0);
        /*=======================================================================*/
        System.out.println("detraccion:"+beanCliente.isDetraccion());
        String DomicilioEntregaJson=gson.toJson(beanDomicilioEntrega);
        //DomicilioEntrega beanDomicilioEntrega1=gson.fromJson(DomicilioEntregaJson, DomicilioEntrega.class);
        System.out.println("codigoPais:"+beanDomicilioEntrega.getCodigoPais());
        System.out.println("direccion:"+beanDomicilioEntrega.getDirección());
        System.out.println("ubigeoCodigo:"+beanDomicilioEntrega.getUbigeoCodigo());
         /*Datos Emisor*/
        System.out.println("Email:"+beanEmisor.getEmail());
        System.out.println("nombreComercial:"+beanEmisor.getNombreComercial());
        System.out.println("numeroDocumento:+"+beanEmisor.getNumeroDocumento());
        System.out.println("razonSocial:"+beanEmisor.getRazonSocial());
        System.out.println("tipoDocumento:"+beanEmisor.getTipoDocumento());
        System.out.println("fechaEmision:"+beanEmisor.getFechaEmision());
        System.out.println("fechaVencimiento:"+beanEmisor.getFechaVencimiento());
        /*====================================*/
        /*Leyenda List*/
        String  LeyendaListJson=gson.toJson(beanLeyendaList);
        System.out.println(LeyendaListJson);
        //LeyendaList beanLeyendaList=gson.fromJson(LeyendaListJson, LeyendaList.class);
        System.out.println("codigo:"+beanLeyendaList.getCodigo());
        System.out.println("descripcion:"+beanLeyendaList.getDescripcion());
        /*===================================================================*/
        System.out.println("montoBaseImponiblePercepcion:"+beanLeyendaList.getMontoBaseImponiblePercepcion());
        System.out.println("montoDescuentos:"+beanLeyendaList.getMontoDescuento());
        System.out.println("montoIGV:"+beanLeyendaList.getMontoIGV());
        System.out.println("montoISC:"+beanLeyendaList.getMontoISC());
        System.out.println("montImporteVenta:"+beanLeyendaList.getMontoImporteVentas());
        System.out.println("montoOperacionesExoneradas:"+beanLeyendaList.getMontoOperacionesExoneradas());
        System.out.println("montoOperacionesGravadas:"+beanLeyendaList.getMontoOperacionesGravadas());
        System.out.println("montoOperacionesInafectas:"+beanLeyendaList.getMontoOperacionesInafectadas());
        System.out.println("montoOtrosTributos:"+beanLeyendaList.getMontoOtrosTributos());
        System.out.println("montoPercepcion:"+beanLeyendaList.getMontoPercepcion());
        System.out.println("montoTotalIncPercepcion:"+beanLeyendaList.getMontoTotalIncPercepcion());
        System.out.println("numero:"+beanLeyendaList.getNumero());
        System.out.println("nuneroComprobante:"+beanLeyendaList.getNumeroComprobante());
        /*ArrayList LeyendaList*/
        ArrayList<LeyendaList>LeyendaListAL=new ArrayList<LeyendaList>();
        LeyendaListAL.add(beanLeyendaList);
        LeyendaListAL.add(new LeyendaList());
        String LeyendaListJsonAL=gson.toJson(LeyendaListAL);
        System.out.println(LeyendaListJsonAL);
        /*============================================================*/
        String RelacionadosListJson=gson.toJson(beanRelacionadoList);
        System.out.println(RelacionadosListJson);
        System.out.println("emisorNumeroDocumento:"+beanRelacionadoList.getEmisorNumeroDocumento());
        System.out.println("emisorTipoDocumento:"+beanRelacionadoList.getEmisorTipoDocumento());
        System.out.println("indicador:"+beanRelacionadoList.getIndicador());
        System.out.println("monto:"+beanRelacionadoList.getMonto());
        System.out.println("numeroDocumento:"+beanRelacionadoList.getNumeroDocumento());
        System.out.println("tipoDocumento:"+beanRelacionadoList.getTipoDocumento());
        /*Array RelacionadoList*/
        ArrayList<RelacionadoLlist>RelacionadosListAL=new ArrayList<RelacionadoLlist>();
        RelacionadosListAL.add(beanRelacionadoList);
        RelacionadosListAL.add(new RelacionadoLlist());
        String RelacionadosJsonAL=gson.toJson(RelacionadosListAL);
        System.out.println(RelacionadosJsonAL);
        /*==================================================================================*/
        System.out.println("serie:"+beanDetalleComprobanteList.getSerie());
        System.out.println("sumaOtrosCargos:"+beanDetalleComprobanteList.getSumaOtrosCargos());
        System.out.println("tipoComprobante:"+beanDetalleComprobanteList.getTipoComprobante());
        System.out.println("tipoMoneda:"+beanDetalleComprobanteList.getTipoMoneda());
        System.out.println("tipoOperacion:"+beanDetalleComprobanteList.getTipoOperacion());
        String jsonRoot = gson.toJson(bean);
        ObjectMapper om = new ObjectMapper();
        om.enable(new SerializationConfig.Feature[]{SerializationConfig.Feature.INDENT_OUTPUT});
        //String json = om.writeValueAsString(gson);
        HttpResponse<JsonNode> res = Unirest.post("http://172.20.0.51:8080/api/facturas").header("Content-Type", "application/json").body(jsonRoot).asJson();
        if (res.getStatus() == 200) {
            JsonNode body = (JsonNode) res.getBody();
            String key = body.getObject().getString("key");
            String hash = body.getObject().getString("digest_value");
            System.out.println("==COMPROBANTE DE PAGO FACTURA==");
            System.out.println("");
            System.out.println("");
            
        }
        
        
       
        
    }
}
