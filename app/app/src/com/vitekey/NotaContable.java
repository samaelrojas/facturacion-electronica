/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vitekey;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gspinka.beans.Boleta;
import com.gspinka.beans.Cliente;
import com.gspinka.beans.Domicilio;
import com.gspinka.beans.DomicilioEntrega;
import com.gspinka.beans.Emisor;
import com.gspinka.beans.Factura;
import com.gspinka.beans.LeyendaList;
import com.gspinka.beans.RelacionadoLlist;
import com.gspinka.beans.Relacionados;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ProcessBuilder.Redirect.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 *
 * @author armando.rojas
 */
public class NotaContable {
    public static void main(String arg[])throws SQLException, ClassNotFoundException, IOException, UnirestException{
        
        /*Conexion*/
         /*=================================Conexion=========================================================================*/
        Properties props = new Properties();
        props.setProperty("user", "scott");
        props.setProperty("password", "tiger");

        Class.forName("oracle.jdbc.driver.OracleDriver");

        Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@172.20.0.7:1521:SIDGPS", "sysgps_demo", "sysgps_demo");

        
        /*==========================*/
        
        Boleta bean=new Boleta();
        Cliente beanCliente=new Cliente();
        Domicilio beanDomicilioCliente=new Domicilio();
        //bean.setDomicilioEntrega(bean.getDomicilioEntrega());
        bean.setDomicilioEntrega(beanDomicilioCliente);
        beanDomicilioCliente.setCodigoPais("+51");
        beanDomicilioCliente.setDirección("Carr Panamericana Norte SN FND fundo iris sub lote 3");
        beanDomicilioCliente.setUbigeoCodigo("14");
        beanCliente.setDomicilio(beanDomicilioCliente);
        bean.setCliente(beanCliente);
        beanCliente.setEmail("armando.rojas@gpsinka.com");
        beanCliente.setNombreComercial("VITAC & RENTA CAR S.A.C");
        beanCliente.setNumeroDocumento("20522727456");
        beanCliente.setRazonSocial("VITAC & RENTA CAR S.A.C");
        beanCliente.setTipoDocumento("Ruc");
        beanCliente.setCodigoMotivo("");
        beanCliente.setDescripcionMotivo("");
        /*==========================================*/
        Boleta beanBoleta= new Boleta();
        beanBoleta.setCantidadUnidadItem(0);
        beanBoleta.setCodigoProducto("");
        beanBoleta.setCodigoProductoSUNAT("");
        beanBoleta.setCodigoTipoPrecioVentaItem("");
        beanBoleta.setCodigoUnidadMedida("");
        beanBoleta.setDescripcionItem("");
        beanBoleta.setDescripcionCDATA("");
        beanBoleta.setLinea(0.0);
        beanBoleta.setMontoDescuentoItem(0.0);
        beanBoleta.setMontoIGVItem(0.0);
        beanBoleta.setMontoISCItem(0.0);
        beanBoleta.setMontoPrecioVentaItem(0.0);
        beanBoleta.setMontoValorUnitario(0.0);
        beanBoleta.setMontoValorUnitarioGratuito(0.0);
        beanBoleta.setMontoValorVentaItem(0.0);
        beanBoleta.setNumeroPlacaItem("");
        beanBoleta.setTipoAfectacionIGV("");
        beanBoleta.setTipocodigoPlaca("");
        beanBoleta.setTipoSistemaCalculoISC("");
        /*==============================================*/
        Domicilio beanDomicilioEntrega = new Domicilio();
        bean.setDomicilioEntrega(beanDomicilioEntrega);
        beanDomicilioEntrega.setCodigoPais("");
        beanDomicilioEntrega.setDirección("");
        beanDomicilioEntrega.setUbigeoCodigo("");
        /*=======================================================================================================*/
        Domicilio beanDomicilioEmisor = new Domicilio();
        bean.setDomicilioEntrega(beanDomicilioEmisor);
        beanDomicilioEmisor.setCodigoPais("+51");
        beanDomicilioEmisor.setDirección("Carr Paranamericana Norte SN FND Fundo Iris Sub Lote 3");
        beanDomicilioEmisor.setUbigeoCodigo("14");
        Emisor beanEmisor = new Emisor();
        bean.setEmisor(beanEmisor);
        beanEmisor.setEmail("imp.genecia@gpsinka.com");
        beanEmisor.setNombreComercial("SOCIEDAD DE AUTOMOTORES INKA S.A.C");
        beanEmisor.setNumeroDocumento("20480683839");
        beanEmisor.setRazonSocial("SOCIEDAD DE AUTOMOTORES INKA S.A.C");
        beanEmisor.setTipoDocumento("RUC");
        beanEmisor.setFechaEmision("2017-12-15");
        LeyendaList beanLeyendaList = new LeyendaList();
        beanLeyendaList.setCodigo("");
        beanLeyendaList.setDescripcion("");
        /*================================================*/
        beanLeyendaList.setMontoDescuento(0.0);
        beanLeyendaList.setMontoIGV(0.0);
        beanLeyendaList.setMontoISC(0.0);
        beanLeyendaList.setMontoImporteVentas(0.0);
        beanLeyendaList.setMontoOperacionesExoneradas(0.0);
        beanLeyendaList.setMontoOperacionesGratuitas(0.0);
        beanLeyendaList.setMontoOperacionesGravadas(0.0);
        beanLeyendaList.setMontoOtrosTributos(0.0);
        beanLeyendaList.setMontoPercepcion(0.0);
        beanLeyendaList.setMontoTotalIncPercepcion(0.0);
        beanLeyendaList.setNumero("");
        beanLeyendaList.setNumeroComprobante("");
        beanLeyendaList.setNumeroDocumentoAfectado("");
        /*============================================*/
        Relacionados beanRelacionados = new Relacionados();
        bean.setRelacionados(beanRelacionados);
        beanRelacionados.setEmisorNumeroDocumento("");
        beanRelacionados.setEmisorTipoDocumento("");
        beanRelacionados.setIndicador("");
        beanRelacionados.setMonto(0.0);
        beanRelacionados.setNumeroDocumento("");
        beanRelacionados.setTipoDocumento("");
        /*=====================================*/
        beanCliente.setSerie("");
        beanCliente.setSumaOtrosCargos(0.0);
        beanCliente.setTieneOperacionesGratuitas(true);
        beanCliente.setTipoComprobante("");
        beanCliente.setTipoDocumento("");
        beanCliente.setTipoMoneda("");
        beanCliente.setTipoOperacion("");
        /*===========================================================================*/
        Gson gson= new Gson();
        String DomicilioJson=gson.toJson(beanDomicilioCliente);
        System.out.println(DomicilioJson);
        //Domicilio beanDomicilioCliente=gson.fromJson(DomicilioJson, Domicilio.class);
        System.out.println("codigoPais:"+ beanDomicilioCliente.getCodigoPais());
        System.out.println("direccion:"+ beanDomicilioCliente.getDirección());
        System.out.println("codigoUbigeo:"+beanDomicilioCliente.getUbigeoCodigo());
        String ClienteJson=gson.toJson(beanCliente);
        System.out.println(ClienteJson);
        //Cliente beanCliente=gson.fromJson(ClienteJson, Cliente.class);
        System.out.println("email:"+ beanCliente.getEmail());
        System.out.println("nombreComercial:"+beanCliente.getNombreComercial());
        System.out.println("numeroDocumento:"+beanCliente.getNumeroDocumento());
        System.out.println("razonSocial:"+beanCliente.getRazonSocial());
        System.out.println("tipoDocumento:"+beanCliente.getTipoDocumento());
        System.out.println("codigoMotivo:"+ beanCliente.getCodigoMotivo());
        System.out.println("descripcionMotivo:"+ beanCliente.getDescripcionMotivo());
        String BoletaJson= gson.toJson(beanBoleta);
        System.out.println(BoletaJson);
        //Boleta beanBoleta=gson.fromJson(BoletaJson, Boleta.class);
        System.out.println("cantidadUnidadItem:"+beanBoleta.getCantidadUnidadItem());
        System.out.println("codigoProducto:"+beanBoleta.getCodigoProducto());
        System.out.println("codigoProductoSUNAT:"+beanBoleta.getCodigoProductoSUNAT());
        System.out.println("codigoTipoPrecioVentaItem:"+beanBoleta.getCodigoTipoPrecioVentaItem());
        System.out.println("codigoUnidadMedida:"+beanBoleta.getCodigoUnidadMedida());
        System.out.println("descripcionItem:"+beanBoleta.getDescripcionItem());
        System.out.println("descripcionItemCDATA:"+beanBoleta.getDescripcionCDATA());
        System.out.println("linea:"+beanBoleta.getLinea());
        System.out.println("montoDescuentoItem:"+beanBoleta.getMontoDescuentoItem());
        System.out.println("montoIGVItem:"+beanBoleta.getMontoIGVItem());
        System.out.println("montoISCItem:"+beanBoleta.getMontoISCItem());
        System.out.println("montoPrecioVentaItem:"+beanBoleta.getMontoPrecioVentaItem());
        System.out.println("montoValorUnitario:"+ beanBoleta.getMontoValorUnitario());
        System.out.println("montoValorUnitarioGratuito:"+beanBoleta.getMontoValorUnitarioGratuito());
        System.out.println("montoValorVentaItem:"+beanBoleta.getMontoValorVentaItem());
        System.out.println("numeroPlacaItem:"+beanBoleta.getNumeroPlacaItem());
        System.out.println("tipoAfectacionIGV:"+beanBoleta.getTipoAfectacionIGV());
        System.out.println("tipoCodigoPlaca:"+ beanBoleta.getTipocodigoPlaca());
        System.out.println("tipoSistemaCalculoISC:"+beanBoleta.getTipoSistemaCalculoISC());
        /*ArrayList Boleta*/
        ArrayList<Boleta>boletaAL=new ArrayList<Boleta>();
        boletaAL.add(beanBoleta);
        boletaAL.add(new Boleta());
        String BoletaJsonAL=gson.toJson(boletaAL);
        System.out.println(BoletaJsonAL);
        /*=================================================================================*/
        String DomicilioEntregaJson=gson.toJson(beanDomicilioEntrega);
        System.out.println(DomicilioEntregaJson);
        //DomicilioEntrega beanDomicilioEntrega=gson.fromJson(DomicilioEntregaJson, DomicilioEntrega.class);
        System.out.println("codigoPais:"+beanDomicilioEntrega.getCodigoPais());
        System.out.println("direccion:"+beanDomicilioEntrega.getDirección());
        System.out.println("ubigeoCodigo:"+beanDomicilioEntrega.getUbigeoCodigo());
        /*=========================================================================*/
        String EmisorJson=gson.toJson(beanEmisor);
        System.out.println(EmisorJson);
        //Emisor beanEmisor=gson.fromJson(EmisorJson, Emisor.class);
        /*DOMICILIO DE EMISOR*/
        String DomicilioEmisorJson= gson.toJson(beanDomicilioEmisor);
        System.out.println(DomicilioEmisorJson);
        Domicilio beanDomicilio=gson.fromJson(DomicilioEmisorJson, Domicilio.class);
        System.out.println("codigoPais:"+beanDomicilio.getCodigoPais());
        System.out.println("direccion:"+beanDomicilio.getDirección());
        System.out.println("ubigeoCodigo:"+beanDomicilio.getUbigeoCodigo());
        /*Datos Emisor*/
        System.out.println("email:"+beanEmisor.getEmail());
        System.out.println("nombreComercial:"+beanEmisor.getNombreComercial());
        System.out.println("numeroDocumento:+"+beanEmisor.getNumeroDocumento());
        System.out.println("razonSocial:"+beanEmisor.getRazonSocial());
        System.out.println("tipoDocumento:"+beanEmisor.getTipoDocumento());
        System.out.println("fechaEmision:"+beanEmisor.getFechaEmision());
        /*Leyenda List*/
        String  LeyendaListJson=gson.toJson(beanLeyendaList);
        System.out.println(LeyendaListJson);
        //LeyendaList beanLeyendaList=gson.fromJson(LeyendaListJson, LeyendaList.class);
        System.out.println("codigo:"+beanLeyendaList.getCodigo());
        System.out.println("descripcion:"+beanLeyendaList.getDescripcion());
        /*===================================================================*/
        System.out.println("montoDescuentos:"+beanLeyendaList.getMontoDescuento());
        System.out.println("montoIGV:"+beanLeyendaList.getMontoIGV());
        System.out.println("montoISC:"+beanLeyendaList.getMontoISC());
        System.out.println("montImporteVenta:"+beanLeyendaList.getMontoImporteVentas());
        System.out.println("montoOperacionesExoneradas:"+beanLeyendaList.getMontoOperacionesExoneradas());
        System.out.println("montoOperacionesGravadas:"+beanLeyendaList.getMontoOperacionesGravadas());
        System.out.println("montoOperacionesInafectas:"+beanLeyendaList.getMontoOperacionesInafectadas());
        System.out.println("montoOtrosTributos:"+beanLeyendaList.getMontoOtrosTributos());
        System.out.println("montoPercepcion:"+beanLeyendaList.getMontoPercepcion());
        System.out.println("montoTotalIncPercepcion:"+beanLeyendaList.getMontoTotalIncPercepcion());
        System.out.println("numero:"+beanLeyendaList.getNumero());
        System.out.println("nuneroComprobante:"+beanLeyendaList.getNumeroComprobante());
        System.out.println("numeroDocumentoAfectado:"+beanLeyendaList.getNumeroDocumentoAfectado());
        /*ArrayList LeyendaList*/
        ArrayList<LeyendaList>LeyendaListAL=new ArrayList<LeyendaList>();
        LeyendaListAL.add(beanLeyendaList);
        LeyendaListAL.add(new LeyendaList());
        String LeyendaListJsonAL=gson.toJson(LeyendaListAL);
        System.out.println(LeyendaListJsonAL);
        
        /*=========================================================================*/
        String RelacionadosJson=gson.toJson(beanRelacionados);
        System.out.println(RelacionadosJson);
        System.out.println("emisorNumeroDocumento:"+beanRelacionados.getEmisorNumeroDocumento());
        System.out.println("emisorTipoDocumento:"+beanRelacionados.getEmisorTipoDocumento());
        System.out.println("indicador:"+beanRelacionados.getIndicador());
        System.out.println("monto:"+beanRelacionados.getMonto());
        System.out.println("numeroDocumento:"+beanRelacionados.getNumeroDocumento());
        System.out.println("tipoDocumento:"+beanRelacionados.getTipoDocumento());
        /*ArrayList Relacionados*/
        ArrayList<Relacionados>RelacionadosAL=new ArrayList<Relacionados>();
        RelacionadosAL.add(beanRelacionados);
        RelacionadosAL.add(new Relacionados());
        String RelacionadosJsonAL=gson.toJson(RelacionadosAL);
        System.out.println(RelacionadosJsonAL);
        /*Serie*/
        System.out.println("serie:"+beanCliente.getSerie());
        System.out.println("sumaOtrosCargos:"+beanCliente.isTieneOperacionesGratuitas());
        System.out.println("tipoComprobante:"+beanCliente.getTipoComprobante());
        System.out.println("tipoDocumentoAfectados:"+beanCliente.getTipoDocumentoAfectado());
        System.out.println("tipoMoneda:"+beanCliente.getTipoMoneda());
        System.out.println("tipoOperacion:"+beanCliente.getTipoOperacion());
        
        String jsonRoot = gson.toJson(bean);
        ObjectMapper om = new ObjectMapper();
        om.enable(new SerializationConfig.Feature[]{SerializationConfig.Feature.INDENT_OUTPUT});
        //String json = om.writeValueAsString(bean);
        HttpResponse<JsonNode> res = Unirest.post("http://172.20.0.51:8080/api/facturas").header("Content-Type", "application/json").body(jsonRoot).asJson();
        if (res.getStatus() == 200) {
            JsonNode body = (JsonNode) res.getBody();
            String key = body.getObject().getString("key");
            String hash = body.getObject().getString("digest_value");
            System.out.println("==COMPROBANTE DE PAGO FACTURA==");
            System.out.println("");
            System.out.println("");
            
        }
        
        
    }
}
