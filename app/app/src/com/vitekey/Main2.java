package com.vitekey;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.RequestBodyEntity;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.json.JSONObject;

public class Main2 {

    public static void main(String[] args)
             throws SQLException, ClassNotFoundException, IOException, UnirestException
  {
    Properties props = new Properties();
    props.setProperty("user", "admin");
    props.setProperty("password", "admin");
    
    Class.forName("oracle.jdbc.driver.OracleDriver");
    
    Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@172.20.0.7:1521:SIDGPS", "SYSGPS_DEMO", "SYSGPS_DEMO");
    
    String sql = "SELECT CDCC_SECUENCIA, NDCC_PEDIDOOIH, cli.E_MAIL, NDCC_SERIE, NDCC_PREIMPRESO, CDCC_TIPODOC, FDCC_EMISION, CDCC_CLIENTE, XDCC_CLIENTE, CDCC_MONEDA,SDCC_STATUS, MDCC_AFECTO, MDCC_NOAFECTO, MDCC_IGV, (MDCC_MONTO + NVL(MDCC_DETRACCION,0)) MONTO_TOTAL, t.NUM1 as IGV, XDCC_CARACTERIS3 FROM DOCUMENTO_CXC LEFT JOIN CLIENTE cli ON cli.COD_CLIENTE = CDCC_CLIENTE INNER JOIN TABLAS t ON t.CATEGORIA = '051' AND t.LLAVE = '03' WHERE CDCC_COMPANIA='01' AND SDCC_STATUS != 'AN' AND XDCC_CARACTERIS3 IS NULL AND SUBSTR(NDCC_SERIE, 1, 1) IN ('E')AND SUBSTR (NDCC_SERIE, 1, 2) IN ('EB') ORDER BY CDCC_SECUENCIA ASC";
    
    PreparedStatement ps_cabecera = conn.prepareStatement(sql);
    
    ResultSet result = ps_cabecera.executeQuery();
    while (result.next())
    {
      String NRO_PEDIDO = result.getString("NDCC_PEDIDOOIH");
      
      HashMap<String, Object> map = new HashMap();
      
      String tipo_documento = "";
      if (result.getString("CDCC_TIPODOC").equals("01")) {
        tipo_documento = "01";
      } else if (result.getString("CDCC_TIPODOC").equals("02")) {
        tipo_documento = "03";
      } else if (result.getString("CDCC_TIPODOC").equals("12")) {
        tipo_documento = "07";
      } else if (result.getString("CDCC_TIPODOC").equals("05")) {
        tipo_documento = "07";
      } else if (result.getString("CDCC_TIPODOC").equals("13")) {
        tipo_documento = "08";
      } else if (result.getString("CDCC_TIPODOC").equals("06")) {
        tipo_documento = "08";
      }
      String nota_tipodoc = "";
      String nota_serie = "";
      String nota_numero = "";
      String nota_motivo = "";
      String nota_codigo = "";
      if ((tipo_documento.equals("07")) || (tipo_documento.equals("08")))
      {
        PreparedStatement ps_docref = conn.prepareStatement("SELECT D.CDCC_COMPANIA, D.CDCC_TIPODOC, D.NDCC_SERIE, D.NDCC_PREIMPRESO, CASE WHEN T.DESCX='NA' THEN T1.LLAVE WHEN T.DESCX='NC' THEN T2.LLAVE END TIPO_MOTIVO, CASE WHEN T.DESCX='NA' THEN T1.DESC1 WHEN T.DESCX='NC' THEN T2.DESC1 END DESC_MOTIVO FROM DEMO_SYSGPS.DOCCXC_REFERENCIA R INNER JOIN DEMO_SYSGPS.DOCUMENTO_CXC D ON R.CDERE_COMPANIAREF = D.CDCC_COMPANIA AND R.CDERE_TIPODOCREF = D.CDCC_TIPODOC AND R.CDERE_SECUENCIAREF = D.CDCC_SECUENCIA INNER JOIN DEMO_SYSGPS.DOCUMENTO_CXC DR ON R.CDERE_COMPANIADCC = DR.CDCC_COMPANIA AND R.CDERE_TIPODOCDCC = DR.CDCC_TIPODOC AND R.CDERE_SECUENCIADCC = DR.CDCC_SECUENCIA INNER JOIN DEMO_SYSGPS.TABLAS T ON T.CATEGORIA='802' AND T.LLAVE=DR.CDCC_TIPODOC LEFT JOIN DEMO_SYSGPS.TABLAS T1 ON T1.CATEGORIA='841' AND T1.LLAVE=DR.CDCC_CODIGO1 LEFT JOIN DEMO_SYSGPS.TABLAS T2 ON T2.CATEGORIA='842' AND T2.LLAVE=DR.CDCC_CODIGO2 WHERE R.CDERE_COMPANIADCC='01' AND CDERE_TIPODOCDCC='" + result.getString("CDCC_TIPODOC") + "' AND CDERE_SECUENCIADCC=" + result.getInt("CDCC_SECUENCIA"));
        
        ResultSet rs_docref = ps_docref.executeQuery();
        rs_docref.next();
        nota_codigo = rs_docref.getString("TIPO_MOTIVO");
        nota_motivo = rs_docref.getString("DESC_MOTIVO");
        if (rs_docref.getString("CDCC_TIPODOC").equals("02")) {
          nota_tipodoc = "03";
        } else {
          nota_tipodoc = "01";
        }
        if (rs_docref.getString("NDCC_SERIE").length() == 3) {
          nota_serie = rs_docref.getString("NDCC_SERIE").substring(0, 1) + "0" + rs_docref.getString("NDCC_SERIE").substring(1);
        } else {
          nota_serie = rs_docref.getString("NDCC_SERIE");
        }
        nota_numero = rs_docref.getString("NDCC_PREIMPRESO");
        
        map.put("tipo_documento_afectado", nota_tipodoc);
        map.put("serie_documento_afectado", nota_serie);
        map.put("numero_documento_afectado", nota_numero);
        map.put("codigo_motivo", nota_codigo);
        map.put("descripcion_motivo", nota_motivo);
      }
      String serie = result.getString("NDCC_SERIE");
      String serie2 = serie.substring(0, 1) + "0" + serie.substring(1);
      String numero = result.getString("NDCC_PREIMPRESO");
      
      map.put("ruc", "20480683839");
      map.put("enviar_sunat", Boolean.valueOf(false));
      map.put("tipo", tipo_documento);
      map.put("serie", serie2);
      map.put("numero", numero);
      map.put("tipo_operacion", "01");
      map.put("fecha_emision", result.getDate("FDCC_EMISION").toString());
      
      String cliente_tipo_doc = "";
      
      String cliente_numero_doc = result.getString("CDCC_CLIENTE");
      if (cliente_numero_doc.length() == 8) {
        cliente_tipo_doc = "1";
      } else if (cliente_numero_doc.length() > 8) {
        if ((cliente_numero_doc.startsWith("10")) || (cliente_numero_doc.startsWith("15")) || (cliente_numero_doc.startsWith("20")))
        {
          cliente_tipo_doc = "6";
        }
        else if (cliente_numero_doc.startsWith("P"))
        {
          cliente_tipo_doc = "4";
        }
        else if (cliente_numero_doc.startsWith("C"))
        {
          cliente_tipo_doc = "7";
        }
        else if (cliente_numero_doc.startsWith("DNI"))
        {
          cliente_tipo_doc = "1";
          cliente_numero_doc = cliente_numero_doc.substring(3);
        }
      }
      map.put("cliente_tipo_doc", cliente_tipo_doc);
      map.put("cliente_numero_doc", cliente_numero_doc);
      map.put("cliente_nombre", result.getString("XDCC_CLIENTE").replace("&", ""));
      
      String cliente_mail = result.getString("E_MAIL");
      if ((cliente_mail != null) && (cliente_mail.length() > 5)) {
        map.put("cliente_email", cliente_mail);
      }
      String row_moneda = result.getString("CDCC_MONEDA");
      if (row_moneda.equals("USD")) {
        map.put("tipo_moneda", "USD");
      } else if (row_moneda.equals("S/.")) {
        map.put("tipo_moneda", "PEN");
      }
      map.put("igv_factor", Float.valueOf(result.getFloat("IGV")));
      
      List<HashMap<String, Object>> lista_detalle = new ArrayList();
      
      PreparedStatement ps_detalle = conn.prepareStatement("SELECT DM.CDEMO_UNIDADART,\n       DM.QDEMO_CANTIDADART,\n       CASE WHEN DM.CDEMO_ITEMART IS NULL THEN UPPER(T1.LLAVE) ELSE DM.CDEMO_ITEMART END CDEMO_ITEMART,\n       CASE WHEN A.DESC_ITEM IS NULL THEN UPPER(T1.DESC1) ELSE A.DESC_ITEM END DESC_ITEM,\n       CASE WHEN (DM.MDEMO_PRECIOART IS NULL OR DM.MDEMO_PRECIOART=0) THEN (DM.MDEMO_AFECTO+DM.MDEMO_NOAFECTO) ELSE DM.MDEMO_PRECIOART END MDEMO_PRECIOART,\n       (DM.MDEMO_PRECIOART * DM.QDEMO_CANTIDADART) AS U,\n       DM.MDEMO_DESCUENTO,\n       DM.MDEMO_IGV,\n       DM.MDEMO_AFECTO,\n       DM.MDEMO_NOAFECTO,\n       (DM.MDEMO_PRECIOART + DM.MDEMO_IGV) PRECIO_IGV_ITEM,\n       DM.MDEMO_PRECIOART * DM.QDEMO_CANTIDADART MONTO_VENTA,\n       A.COD_ITEM\n  FROM    DEMO_SYSGPS.DOCCXC_MOTIVO DM\n       LEFT JOIN\n          DEMO_SYSGPS.ARTICULOS A\n       ON DM.CDEMO_COMPANIAART = A.COD_CIA AND DM.CDEMO_ITEMART = A.COD_ITEM\n       LEFT JOIN TABLAS T1 ON T1.CATEGORIA='806' AND T1.LLAVE=DM.CDEMO_MOTIVO\n WHERE     DM.CDEMO_COMPANIADCC = '01'\n       AND DM.CDEMO_TIPODOCDCC = '" + result
      
        .getString("CDCC_TIPODOC") + "'\n" + "       AND DM.CDEMO_SECUENCIADCC = " + result
        .getInt("CDCC_SECUENCIA"));
      
      ResultSet result_detalle = ps_detalle.executeQuery();
      while (result_detalle.next())
      {
        HashMap<String, Object> map_detalle = new HashMap();
        
        String COD_ITEM = result_detalle.getString("COD_ITEM");
        if ((result.getString("NDCC_PEDIDOOIH") != null) && (result.getString("NDCC_PEDIDOOIH").startsWith("PEV")))
        {
          PreparedStatement ps_marca = conn.prepareStatement("SELECT T.DESC1 FROM ARTICULOS A LEFT JOIN TABLAS T ON T.CATEGORIA='301' AND SUBSTR(T.LLAVE,1,3)=SUBSTR(A.CARACT1,1,3) WHERE A.COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_marca = ps_marca.executeQuery();
          rs_marca.next();
          PreparedStatement ps_serie = conn.prepareStatement("SELECT PD.VIN FROM PVA_PEDIDO_HEADER PD WHERE COMPANIA_VENTA='01' AND NRO_PEDIDO='" + NRO_PEDIDO + "'");
          ResultSet rs_serie = ps_serie.executeQuery();
          rs_serie.next();
          PreparedStatement ps_motor = conn.prepareStatement("SELECT PD.MOTOR FROM PVA_PEDIDO_HEADER PD WHERE COMPANIA_VENTA='01' AND NRO_PEDIDO='" + NRO_PEDIDO + "'");
          ResultSet rs_motor = ps_motor.executeQuery();
          rs_motor.next();
          PreparedStatement ps_modelo = conn.prepareStatement("SELECT NVL(T.DESC1,'') as MODELO FROM ARTICULOS A LEFT JOIN TABLAS T ON T.CATEGORIA='303' AND SUBSTR(T.LLAVE,1,3)=SUBSTR(A.CARACT3,1,3) WHERE A.COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_modelo = ps_modelo.executeQuery();
          rs_modelo.next();
          PreparedStatement ps_aniofab = conn.prepareStatement("SELECT NVL(SUBSTR(T.DESC1,1,4),'') AS ANIO FROM ARTICULOS A LEFT JOIN TABLAS T ON T.CATEGORIA='302' AND SUBSTR(T.LLAVE,1,3)=SUBSTR(A.CARACT2,1,3) WHERE A.COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_aniofab = ps_aniofab.executeQuery();
          rs_aniofab.next();
          PreparedStatement ps_aniomodelo = conn.prepareStatement("SELECT NVL(SUBSTR(T.DESC1,6,4),'') AS ANIO_MODELO FROM ARTICULOS A LEFT JOIN TABLAS T ON T.CATEGORIA='302' AND SUBSTR(T.LLAVE,1,3)=SUBSTR(A.CARACT2,1,3) WHERE A.COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_aniomodelo = ps_aniomodelo.executeQuery();
          rs_aniomodelo.next();
          PreparedStatement ps_color = conn.prepareStatement("SELECT C.DESCRIPCION AS COLOR FROM PVA_PEDIDO_HEADER P INNER JOIN SAS_COLOR C ON C.ID_COLOR=P.ID_COLOR WHERE P.COMPANIA_VENTA='01' AND P.NRO_PEDIDO='" + NRO_PEDIDO + "'");
          ResultSet rs_color = ps_color.executeQuery();
          rs_color.next();
          PreparedStatement ps_vendedor = conn.prepareStatement("SELECT (T.LLAVE||' - '|| T.DESC1) AS VENDEDOR FROM PVA_PEDIDO_HEADER P INNER JOIN TABLAS T ON T.LLAVE=P.COD_VENDEDOR WHERE P.COMPANIA_VENTA='01' AND T.CATEGORIA='030' AND T.NUM1=1448 AND P.NRO_PEDIDO='" + NRO_PEDIDO + "'");
          ResultSet rs_vendedor = ps_vendedor.executeQuery();
          rs_vendedor.next();
          PreparedStatement ps_cilindro = conn.prepareStatement("SELECT VC.NUM_CILINDRO,VC.POTENCIA, VC.NUM_CILINDRADA, COM.DESCRIPCION COMBUSTIBLE, VC.NUM_ASIENTO, VC.NUM_RUEDA, VC.CATEGORIA, VC.NUM_EJE, VC.FORM_RODANTE FROM PVA_VEHICULO_CARACT VC  INNER JOIN SAS_COMBuSTIBLE COM ON COM.ID_COMBUSTIBLE=VC.ID_COMBUSTIBLE  WHERE VC.COMPANIA_VENTA='01' AND VC.COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_cilindro = ps_cilindro.executeQuery();
          boolean h_cilindro = rs_cilindro.next();
          PreparedStatement ps_carroceria = conn.prepareStatement("SELECT T.DESC1 FROM ARTICULOS A LEFT JOIN TABLAS T ON T.CATEGORIA='016' AND T.LLAVE = A.COD_LINEA || A.CLASE || A.SUBCLASE WHERE A.COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_carroceria = ps_carroceria.executeQuery();
          rs_carroceria.next();
          PreparedStatement ps_altura = conn.prepareStatement("SELECT ALTO FROM ARTICULOS WHERE COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_altura = ps_altura.executeQuery();
          rs_altura.next();
          PreparedStatement ps_ancho = conn.prepareStatement("SELECT ANCHO FROM ARTICULOS WHERE COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_ancho = ps_ancho.executeQuery();
          rs_ancho.next();
          PreparedStatement ps_largo = conn.prepareStatement("SELECT LARGO FROM ARTICULOS WHERE COD_ITEM='" + COD_ITEM + "'");
          ResultSet rs_largo = ps_largo.executeQuery();
          rs_largo.next();
          if (COD_ITEM != null)
          {
            StringBuilder referencia_producto = new StringBuilder();
            
            referencia_producto.append("MARCA: " + rs_marca.getString("DESC1") + " \n");
            referencia_producto.append("SERIE: " + rs_serie.getString("VIN") + "\n");
            referencia_producto.append("MOTOR: " + rs_motor.getString("MOTOR") + "\n");
            referencia_producto.append("MODELO: " + rs_modelo.getString("MODELO") + "\n");
            referencia_producto.append("ANIO FABRICACION: " + rs_aniofab.getString("ANIO") + "\n");
            referencia_producto.append("COLOR: " + rs_color.getString("COLOR") + "\n");
            if (h_cilindro)
            {
              referencia_producto.append("N CILINDRO: " + rs_cilindro.getString("NUM_CILINDRO") + "\n");
              referencia_producto.append("CILINDRADA: " + rs_cilindro.getString("NUM_CILINDRADA") + "\n");
              referencia_producto.append("COMBUSTIBLE: " + rs_cilindro.getString("COMBUSTIBLE") + "\n");
              referencia_producto.append("N ASIENTOS: " + rs_cilindro.getString("NUM_ASIENTO") + "\n");
              referencia_producto.append("N RUEDAS: " + rs_cilindro.getString("NUM_RUEDA") + "\n");
              referencia_producto.append("CATEGORIA: " + rs_cilindro.getString("CATEGORIA") + "\n");
            }
            referencia_producto.append("CARROCERIA: " + rs_carroceria.getString(1) + "\n");
            if (h_cilindro)
            {
              referencia_producto.append("POTENCIA: " + rs_cilindro.getString("POTENCIA") + "\n");
              referencia_producto.append("N EJES: " + rs_cilindro.getString("NUM_EJE") + "\n");
              referencia_producto.append("FORM RODANTE: " + rs_cilindro.getString("FORM_RODANTE") + "\n");
            }
            referencia_producto.append("VENDEDOR: " + rs_vendedor.getString(1) + "\n");
            referencia_producto.append("PEDIDO: " + NRO_PEDIDO + "\n");
            
            map_detalle.put("referencia_producto", referencia_producto.toString());
          }
        }
        map_detalle.put("codigo_unidad_medida", "NIU");
        map_detalle.put("cantidad", result_detalle.getString("QDEMO_CANTIDADART"));
        map_detalle.put("codigo_producto", result_detalle.getString("CDEMO_ITEMART"));
        map_detalle.put("descripcion_producto", result_detalle.getString("DESC_ITEM"));
        map_detalle.put("valor_unitario", result_detalle.getString("MDEMO_PRECIOART"));
        map_detalle.put("tipo_igv", "10");
        map_detalle.put("descuento", result_detalle.getString("MDEMO_DESCUENTO"));
        lista_detalle.add(map_detalle);
      }
      map.put("detalle", lista_detalle);
      
      ObjectMapper om = new ObjectMapper();
      om.enable(new SerializationConfig.Feature[] { SerializationConfig.Feature.INDENT_OUTPUT });
      String json = om.writeValueAsString(map);
      
      
      //HttpResponse<JsonNode> res = Unirest.post("http://facturacion.vitekey.com/api/comprobantes/enviar").header("x-api-token", "b6b13990838972569e62de2bd3cf2bb6d8f16c1d867aa203a870d5bcf6cca645").header("x-api-produccion", "true").header("Content-Type", "application/json").body(json).asJson();
      HttpResponse<JsonNode> res = Unirest.post("http://172.20.0.51:8080/api/factura").header("x-api-token", "b6b13990838972569e62de2bd3cf2bb6d8f16c1d867aa203a870d5bcf6cca645").header("x-api-produccion", "true").header("Content-Type", "application/json").body(json).asJson();
      if (res.getStatus() == 200)
      {
        JsonNode body = (JsonNode)res.getBody();
        String key = body.getObject().getString("key");
        String hash = body.getObject().getString("digest_value");
        
        String sunat_codigo = null;
        
        String sunat_descripcion = null;
        
        System.out.println("=== COMPROBANTE ENVIADO ENVIADA");
        System.out.println("- TIPO: " + tipo_documento);
        System.out.println("- SERIE: " + serie);
        System.out.println("- NUMERO: " + numero);
        System.out.println("- KEY: " + key);
        
        PreparedStatement st_update = conn.prepareStatement("UPDATE DOCUMENTO_CXC SET XDCC_CARACTERIS3 = '" + key + "', XDCC_CARACTERIS4 = '" + hash + "', XDCC_CARACTERIS5 = '" + sunat_codigo + "', XDCC_CARACTERIS6 = '" + sunat_descripcion + "' WHERE  CDCC_SECUENCIA = " + result.getInt("CDCC_SECUENCIA"));
        boolean updated = st_update.execute();
        if (updated) {
          System.out.println("ACTUALIZADO EN BD");
        }
      }
      else
      {
        System.out.println("Ocurrio un error");
        System.out.println(((JsonNode)res.getBody()).toString());
      }
    }
  }
}
