
package com.gspinka.beans;

import java.io.Serializable;

/**
 *
 * @author armando.rojas
 */
public class Domicilio implements Serializable{
    
    private String codigoPais;
    private String dirección;
    private String ubigeoCodigo;

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getDirección() {
        return dirección;
    }

    public void setDirección(String dirección) {
        this.dirección = dirección;
    }

    public String getUbigeoCodigo() {
        return ubigeoCodigo;
    }

    public void setUbigeoCodigo(String ubigeoCodigo) {
        this.ubigeoCodigo = ubigeoCodigo;
    }
    
    
    
}
