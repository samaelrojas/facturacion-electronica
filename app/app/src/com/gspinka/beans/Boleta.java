/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gspinka.beans;

import java.io.Serializable;
import java.util.ArrayList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author armando.rojas
 */
public class Boleta implements Serializable{
    private ArrayList<Boleta>boleta;
    private Emisor emisor;
    private Cliente cliente;
    private int cantidadUnidadItem;
    private String codigoProducto;
    private String codigoProductoSUNAT;
    private String codigoTipoPrecioVentaItem;
    private String codigoUnidadMedida;
    private String descripcionItem;
    private String descripcionCDATA;
    private Double linea;
    private Double montoDescuentoItem;
    private Double montoIGVItem;
    private Double montoISCItem;
    private Double montoPrecioVentaItem;
    private Double montoValorUnitario;
    private Double montoValorUnitarioGratuito;
    private Double montoValorVentaItem;
    private String numeroPlacaItem;
    private String tipoAfectacionIGV;
    private String tipocodigoPlaca;
    private String tipoSistemaCalculoISC;
    private Domicilio domicilioEntrega;
    private Relacionados relacionados;

    public Relacionados getRelacionados() {
        return relacionados;
    }

    public void setRelacionados(Relacionados relacionados) {
        this.relacionados = relacionados;
    }
    
    public Domicilio getDomicilioEntrega() {
        return domicilioEntrega;
    }

    public void setDomicilioEntrega(Domicilio domicilioEntrega) {
        this.domicilioEntrega = domicilioEntrega;
    }

    public Emisor getEmisor() {
        return emisor;
    }

    public void setEmisor(Emisor emisor) {
        this.emisor = emisor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Boleta> getBoleta() {
        return boleta;
    }

    public void setBoleta(ArrayList<Boleta> boleta) {
        this.boleta = boleta;
    }

    public int getCantidadUnidadItem() {
        return cantidadUnidadItem;
    }

    public void setCantidadUnidadItem(int cantidadUnidadItem) {
        this.cantidadUnidadItem = cantidadUnidadItem;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getCodigoProductoSUNAT() {
        return codigoProductoSUNAT;
    }

    public void setCodigoProductoSUNAT(String codigoProductoSUNAT) {
        this.codigoProductoSUNAT = codigoProductoSUNAT;
    }

    public String getCodigoTipoPrecioVentaItem() {
        return codigoTipoPrecioVentaItem;
    }

    public void setCodigoTipoPrecioVentaItem(String codigoTipoPrecioVentaItem) {
        this.codigoTipoPrecioVentaItem = codigoTipoPrecioVentaItem;
    }

    public String getCodigoUnidadMedida() {
        return codigoUnidadMedida;
    }

    public void setCodigoUnidadMedida(String codigoUnidadMedida) {
        this.codigoUnidadMedida = codigoUnidadMedida;
    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public String getDescripcionCDATA() {
        return descripcionCDATA;
    }

    public void setDescripcionCDATA(String descripcionCDATA) {
        this.descripcionCDATA = descripcionCDATA;
    }

    public Double getLinea() {
        return linea;
    }

    public void setLinea(Double linea) {
        this.linea = linea;
    }

    public Double getMontoDescuentoItem() {
        return montoDescuentoItem;
    }

    public void setMontoDescuentoItem(Double montoDescuentoItem) {
        this.montoDescuentoItem = montoDescuentoItem;
    }

    public Double getMontoIGVItem() {
        return montoIGVItem;
    }

    public void setMontoIGVItem(Double montoIGVItem) {
        this.montoIGVItem = montoIGVItem;
    }

    public Double getMontoISCItem() {
        return montoISCItem;
    }

    public void setMontoISCItem(Double montoISCItem) {
        this.montoISCItem = montoISCItem;
    }

    public Double getMontoPrecioVentaItem() {
        return montoPrecioVentaItem;
    }

    public void setMontoPrecioVentaItem(Double montoPrecioVentaItem) {
        this.montoPrecioVentaItem = montoPrecioVentaItem;
    }

    public Double getMontoValorUnitario() {
        return montoValorUnitario;
    }

    public void setMontoValorUnitario(Double montoValorUnitario) {
        this.montoValorUnitario = montoValorUnitario;
    }

    public Double getMontoValorUnitarioGratuito() {
        return montoValorUnitarioGratuito;
    }

    public void setMontoValorUnitarioGratuito(Double montoValorUnitarioGratuito) {
        this.montoValorUnitarioGratuito = montoValorUnitarioGratuito;
    }

    public Double getMontoValorVentaItem() {
        return montoValorVentaItem;
    }

    public void setMontoValorVentaItem(Double montoValorVentaItem) {
        this.montoValorVentaItem = montoValorVentaItem;
    }

    public String getNumeroPlacaItem() {
        return numeroPlacaItem;
    }

    public void setNumeroPlacaItem(String numeroPlacaItem) {
        this.numeroPlacaItem = numeroPlacaItem;
    }

    public String getTipoAfectacionIGV() {
        return tipoAfectacionIGV;
    }

    public void setTipoAfectacionIGV(String tipoAfectacionIGV) {
        this.tipoAfectacionIGV = tipoAfectacionIGV;
    }

    public String getTipocodigoPlaca() {
        return tipocodigoPlaca;
    }

    public void setTipocodigoPlaca(String tipocodigoPlaca) {
        this.tipocodigoPlaca = tipocodigoPlaca;
    }

    public String getTipoSistemaCalculoISC() {
        return tipoSistemaCalculoISC;
    }

    public void setTipoSistemaCalculoISC(String tipoSistemaCalculoISC) {
        this.tipoSistemaCalculoISC = tipoSistemaCalculoISC;
    }
   /*
    public ResultSet listar() throws Exception{
        String sql = "SELECT CDCC_SECUENCIA, NDCC_PEDIDOOIH, cli.E_MAIL, NDCC_SERIE, NDCC_PREIMPRESO, CDCC_TIPODOC, FDCC_EMISION, CDCC_CLIENTE, XDCC_CLIENTE, CDCC_MONEDA,SDCC_STATUS, MDCC_AFECTO, MDCC_NOAFECTO, MDCC_IGV, (MDCC_MONTO + NVL(MDCC_DETRACCION,0)) MONTO_TOTAL, t.NUM1 as IGV, XDCC_CARACTERIS3 FROM DOCUMENTO_CXC LEFT JOIN CLIENTE cli ON cli.COD_CLIENTE = CDCC_CLIENTE INNER JOIN TABLAS t ON t.CATEGORIA = '051' AND t.LLAVE = '03' WHERE CDCC_COMPANIA='01' AND SDCC_STATUS != 'AN' AND XDCC_CARACTERIS3 IS NULL  ORDER BY CDCC_SECUENCIA ASC";
        ResultSet resultado = this.ejecutarSQL(sql);
        return resultado;
    }
    */
}
