/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gspinka.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author armando.rojas
 */
public class Relacionados implements Serializable{
    private ArrayList<Relacionados>relacionados;
    private String emisorNumeroDocumento;
    private String emisorTipoDocumento;
    private String indicador;
    private Double monto;
    private String numeroDocumento;
    private String tipoDocumento;

    
    public ArrayList<Relacionados> getRelacionados() {
        return relacionados;
    }

    public void setRelacionados(ArrayList<Relacionados> relacionados) {
        this.relacionados = relacionados;
    }

    public String getEmisorNumeroDocumento() {
        return emisorNumeroDocumento;
    }

    public void setEmisorNumeroDocumento(String emisorNumeroDocumento) {
        this.emisorNumeroDocumento = emisorNumeroDocumento;
    }

    public String getEmisorTipoDocumento() {
        return emisorTipoDocumento;
    }

    public void setEmisorTipoDocumento(String emisorTipoDocumento) {
        this.emisorTipoDocumento = emisorTipoDocumento;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
}
