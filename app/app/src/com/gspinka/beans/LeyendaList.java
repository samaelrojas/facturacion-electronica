/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gspinka.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author armando.rojas
 */
public class LeyendaList implements Serializable {

    private String codigo;
    private String descripcion;
    private ArrayList<LeyendaList> leyendaList;
    private Double montoBaseImponiblePercepcion;
    private Double montoDescuento;
    private Double montoIGV;
    private Double montoISC;
    private Double montoImporteVentas;
    private Double montoOperacionesExoneradas;
    private Double montoOperacionesGratuitas;
    private Double montoOperacionesGravadas;
    private Double montoOperacionesInafectadas;
    private Double montoOtrosTributos;
    private Double montoPercepcion;
    private Double montoTotalIncPercepcion;
    //============================================//
    private String numero;
    private String numeroComprobante;
    private String numeroDocumentoAfectado;

    public String getNumeroDocumentoAfectado() {
        return numeroDocumentoAfectado;
    }

    public void setNumeroDocumentoAfectado(String numeroDocumentoAfectado) {
        this.numeroDocumentoAfectado = numeroDocumentoAfectado;
    }
    

    public Double getMontoIGV() {
        return montoIGV;
    }

    public void setMontoIGV(Double montoIGV) {
        this.montoIGV = montoIGV;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<LeyendaList> getLeyendaList() {
        return leyendaList;
    }

    public void setLeyendaList(ArrayList<LeyendaList> leyendaList) {
        this.leyendaList = leyendaList;
    }

    public Double getMontoBaseImponiblePercepcion() {
        return montoBaseImponiblePercepcion;
    }

    public void setMontoBaseImponiblePercepcion(Double montoBaseImponiblePercepcion) {
        this.montoBaseImponiblePercepcion = montoBaseImponiblePercepcion;
    }

    public Double getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(Double montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public Double getMontoISC() {
        return montoISC;
    }

    public void setMontoISC(Double montoISC) {
        this.montoISC = montoISC;
    }

    public Double getMontoImporteVentas() {
        return montoImporteVentas;
    }

    public void setMontoImporteVentas(Double montoImporteVentas) {
        this.montoImporteVentas = montoImporteVentas;
    }

    public Double getMontoOperacionesExoneradas() {
        return montoOperacionesExoneradas;
    }

    public void setMontoOperacionesExoneradas(Double montoOperacionesExoneradas) {
        this.montoOperacionesExoneradas = montoOperacionesExoneradas;
    }

    public Double getMontoOperacionesGratuitas() {
        return montoOperacionesGratuitas;
    }

    public void setMontoOperacionesGratuitas(Double montoOperacionesGratuitas) {
        this.montoOperacionesGratuitas = montoOperacionesGratuitas;
    }

    public Double getMontoOperacionesGravadas() {
        return montoOperacionesGravadas;
    }

    public void setMontoOperacionesGravadas(Double montoOperacionesGravadas) {
        this.montoOperacionesGravadas = montoOperacionesGravadas;
    }

    public Double getMontoOperacionesInafectadas() {
        return montoOperacionesInafectadas;
    }

    public void setMontoOperacionesInafectadas(Double montoOperacionesInafectadas) {
        this.montoOperacionesInafectadas = montoOperacionesInafectadas;
    }

    public Double getMontoOtrosTributos() {
        return montoOtrosTributos;
    }

    public void setMontoOtrosTributos(Double montoOtrosTributos) {
        this.montoOtrosTributos = montoOtrosTributos;
    }

    public Double getMontoPercepcion() {
        return montoPercepcion;
    }

    public void setMontoPercepcion(Double montoPercepcion) {
        this.montoPercepcion = montoPercepcion;
    }

    public Double getMontoTotalIncPercepcion() {
        return montoTotalIncPercepcion;
    }

    public void setMontoTotalIncPercepcion(Double montoTotalIncPercepcion) {
        this.montoTotalIncPercepcion = montoTotalIncPercepcion;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }
    
    
    
}
