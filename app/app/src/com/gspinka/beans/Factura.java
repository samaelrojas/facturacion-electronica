/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gspinka.beans;

import java.io.Serializable;

/**
 *
 * @author armando.rojas
 */
public class Factura implements Serializable{
    private Cliente cliente;
    private Emisor emisor;
    private String email;
    private String nombreComercial;
    private String numeroDocumento;
    private String razonSocial;
    private String tipoDocumento;
    private boolean detraccion=true;
    private Domicilio domicilioEntrega;
    private LeyendaList leyendaList;
    private RelacionadoLlist relacionadoList;
    private DetalleComprobanteList detalleComprobanteList;

    public DetalleComprobanteList getDetalleComprobanteList() {
        return detalleComprobanteList;
    }

    public void setDetalleComprobanteList(DetalleComprobanteList detalleComprobanteList) {
        this.detalleComprobanteList = detalleComprobanteList;
    }
    
    
    public RelacionadoLlist getRelacionadoList() {
        return relacionadoList;
    }

    public void setRelacionadoList(RelacionadoLlist relacionadoList) {
        this.relacionadoList = relacionadoList;
    }
    
    
    public LeyendaList getLeyendaList() {
        return leyendaList;
    }

    public void setLeyendaList(LeyendaList leyendaList) {
        this.leyendaList = leyendaList;
    }
    
    public Emisor getEmisor() {
        return emisor;
    }

    public void setEmisor(Emisor emisor) {
        this.emisor = emisor;
    }
    public Domicilio getDomicilioEntrega() {
        return domicilioEntrega;
    }

    public void setDomicilioEntrega(Domicilio domicilioEntrega) {
        this.domicilioEntrega = domicilioEntrega;
    }
    
    public boolean isDetraccion() {
        return detraccion;
    }

    public void setDetraccion(boolean detraccion) {
        this.detraccion = detraccion;
    }
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

   
}
