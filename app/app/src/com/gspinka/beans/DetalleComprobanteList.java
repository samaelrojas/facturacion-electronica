/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gspinka.beans;

import java.awt.List;
import java.io.Serializable;
/**
 *
 * @author armando.rojas
 */
import java.util.ArrayList;
public class DetalleComprobanteList implements Serializable{
    private ArrayList<DetalleComprobanteList>detalleComprobanteList;
    private int cantidadUnidadItem;
    private String codigoProducto;
    private String codigoProductoSUNAT;
    private String codigoTipoPrecioVentaItem;
    private String codigoUnidadMedida;
    private String descripcionItem;
    private String descripcionItemCDATA;
    private int linea;
    private Double montoDescuentoItem;
    private Double montoIGVItem;
    private Double montoSCItem;
    private Double montoPrecioVentaItem;
    private Double montoValorUnitario;
    private Double montoValorUnitarioGratuito;
    private Double montoValorVentaItem;
    private String numeroPlacaItem;
    private String montoPlacaItem;
    private String tipoAfectacionIGV;
    private String tipoCodigoPlaca;
    private String tipoSistemaCalculoISC;
    private String serie ;
    private Double sumaOtrosCargos;
    private String sumaOperacionesGratuitas;
    private String tipoComprobante;
    private String tipoMoneda;
    private String numeroDocumento;
    private String tipoOperacion;

    public String getNumeroPlacaItem() {
        return numeroPlacaItem;
    }

    public void setNumeroPlacaItem(String numeroPlacaItem) {
        this.numeroPlacaItem = numeroPlacaItem;
    }

   

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Double getSumaOtrosCargos() {
        return sumaOtrosCargos;
    }

    public void setSumaOtrosCargos(Double sumaOtrosCargos) {
        this.sumaOtrosCargos = sumaOtrosCargos;
    }

    public String getSumaOperacionesGratuitas() {
        return sumaOperacionesGratuitas;
    }

    public void setSumaOperacionesGratuitas(String sumaOperacionesGratuitas) {
        this.sumaOperacionesGratuitas = sumaOperacionesGratuitas;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
   
    public String getTipoAfectacionIGV() {
        return tipoAfectacionIGV;
    }

    public void setTipoAfectacionIGV(String tipoAfectacionIGV) {
        this.tipoAfectacionIGV = tipoAfectacionIGV;
    }
    public Double getMontoValorVentaItem() {
        return montoValorVentaItem;
    }

    public void setMontoValorVentaItem(Double montoValorVentaItem) {
        this.montoValorVentaItem = montoValorVentaItem;
    }

    //private ArrayList<LeyendaList>leyendaList;
    public ArrayList<DetalleComprobanteList> getDetalleComprobanteList() {
        return detalleComprobanteList;
    }

    public void setDetalleComprobanteList(ArrayList<DetalleComprobanteList> detalleComprobanteList) {
        this.detalleComprobanteList = detalleComprobanteList;
    }

    public int getCantidadUnidadItem() {
        return cantidadUnidadItem;
    }

    public void setCantidadUnidadItem(int cantidadUnidadItem) {
        this.cantidadUnidadItem = cantidadUnidadItem;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getCodigoProductoSUNAT() {
        return codigoProductoSUNAT;
    }

    public void setCodigoProductoSUNAT(String codigoProductoSUNAT) {
        this.codigoProductoSUNAT = codigoProductoSUNAT;
    }

    public String getCodigoTipoPrecioVentaItem() {
        return codigoTipoPrecioVentaItem;
    }

    public void setCodigoTipoPrecioVentaItem(String codigoTipoPrecioVentaItem) {
        this.codigoTipoPrecioVentaItem = codigoTipoPrecioVentaItem;
    }

    public String getCodigoUnidadMedida() {
        return codigoUnidadMedida;
    }

    public void setCodigoUnidadMedida(String codigoUnidadMedida) {
        this.codigoUnidadMedida = codigoUnidadMedida;
    }

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }

    public String getDescripcionItemCDATA() {
        return descripcionItemCDATA;
    }

    public void setDescripcionItemCDATA(String descripcionItemCDATA) {
        this.descripcionItemCDATA = descripcionItemCDATA;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public Double getMontoDescuentoItem() {
        return montoDescuentoItem;
    }

    public void setMontoDescuentoItem(Double montoDescuentoItem) {
        this.montoDescuentoItem = montoDescuentoItem;
    }

    public Double getMontoIGVItem() {
        return montoIGVItem;
    }

    public void setMontoIGVItem(Double montoIGVItem) {
        this.montoIGVItem = montoIGVItem;
    }

    public Double getMontoSCItem() {
        return montoSCItem;
    }

    public void setMontoSCItem(Double montoSCItem) {
        this.montoSCItem = montoSCItem;
    }

    public Double getMontoPrecioVentaItem() {
        return montoPrecioVentaItem;
    }

    public void setMontoPrecioVentaItem(Double montoPrecioVentaItem) {
        this.montoPrecioVentaItem = montoPrecioVentaItem;
    }

    public Double getMontoValorUnitario() {
        return montoValorUnitario;
    }

    public void setMontoValorUnitario(Double montoValorUnitario) {
        this.montoValorUnitario = montoValorUnitario;
    }

    public Double getMontoValorUnitarioGratuito() {
        return montoValorUnitarioGratuito;
    }

    public void setMontoValorUnitarioGratuito(Double montoValorUnitarioGratuito) {
        this.montoValorUnitarioGratuito = montoValorUnitarioGratuito;
    }

    public String getMontoPlacaItem() {
        return montoPlacaItem;
    }

    public void setMontoPlacaItem(String montoPlacaItem) {
        this.montoPlacaItem = montoPlacaItem;
    }
    public String getTipoCodigoPlaca() {
        return tipoCodigoPlaca;
    }

    public void setTipoCodigoPlaca(String tipoCodigoPlaca) {
        this.tipoCodigoPlaca = tipoCodigoPlaca;
    }

    public String getTipoSistemaCalculoISC() {
        return tipoSistemaCalculoISC;
    }

    public void setTipoSistemaCalculoISC(String tipoSistemaCalculoISC) {
        this.tipoSistemaCalculoISC = tipoSistemaCalculoISC;
    }
    
}
