/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gspinka.beans;

import java.io.Serializable;

/**
 *
 * @author armando.rojas
 */
public class Cliente implements Serializable{
    private Domicilio domicilio;
    private String email;
    private String nombreComercial;
    private String numeroDocumento;
    private String razonSocial;
    private String tipoDocumento;
    private String codigoMotivo;
    private String descripcionMotivo;
    /*=================================*/
    private String codigoPercepcion;
    private String codigoRegistroPercepcion;
    /*===============================*/
    private String serie;
    private Double sumaOtrosCargos;
    private boolean tieneOperacionesGratuitas=true;
    private String tipoComprobante;
    private String tipoDocumentoAfectado;
    private String tipoMoneda;
    private String tipoOperacion;

    public String getCodigoPercepcion() {
        return codigoPercepcion;
    }

    public void setCodigoPercepcion(String codigoPercepcion) {
        this.codigoPercepcion = codigoPercepcion;
    }

    public String getCodigoRegistroPercepcion() {
        return codigoRegistroPercepcion;
    }

    public void setCodigoRegistroPercepcion(String codigoRegistroPercepcion) {
        this.codigoRegistroPercepcion = codigoRegistroPercepcion;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Double getSumaOtrosCargos() {
        return sumaOtrosCargos;
    }

    public void setSumaOtrosCargos(Double sumaOtrosCargos) {
        this.sumaOtrosCargos = sumaOtrosCargos;
    }

    public boolean isTieneOperacionesGratuitas() {
        return tieneOperacionesGratuitas;
    }

    public void setTieneOperacionesGratuitas(boolean tieneOperacionesGratuitas) {
        this.tieneOperacionesGratuitas = tieneOperacionesGratuitas;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public String getTipoDocumentoAfectado() {
        return tipoDocumentoAfectado;
    }

    public void setTipoDocumentoAfectado(String tipoDocumentoAfectado) {
        this.tipoDocumentoAfectado = tipoDocumentoAfectado;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    
    

    public String getDescripcionMotivo() {
        return descripcionMotivo;
    }

    public void setDescripcionMotivo(String descripcionMotivo) {
        this.descripcionMotivo = descripcionMotivo;
    }
    

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    private boolean detraccion=true;

    public boolean isDetraccion() {
        return detraccion;
    }

    public void setDetraccion(boolean detraccion) {
        this.detraccion = detraccion;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }
    
    
}
